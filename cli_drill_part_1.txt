

1.) Drill 1


hello
├── five
│   └── six
│       ├── c.txt
│       └── seven
│           └── error.log
└── one
    ├── a.txt
    ├── b.txt
    └── two
        ├── d.txt
        └── three
            ├── e.txt
            └── four
                └── access.log

1.1) *******************************************************************************


> mkdir hello  #It will make a new directory named "hello"

	> cd hello #Directory change to hello from parent Directory

	> mkdir five # It will make a new directory inside hello named "five"

		> cd five # Directory change to "five" from parent Directory ("hello")

		> mkdir six #It will make a new directory inside five named "six"

			> cd six # Directory change to six from parent Directory

			> touch c.txt #It will make a empty text file named "c" inside "six" Directory

			> mkdir seven #It will make a new directory inside "six" named "seven"

				> cd seven  #Directory change to "seven" from parent Directory.

				> touch error.log  #It will make a empty log file named as "error" inside seven

> cd ..//..//..//  # It change the current directory to "hello" from directory "seven"

> mkdir one #It will make a new directory inside hello named "one"

	> cd one #Directory change to "one" from parent Directory

	> touch a.txt #It create a empty text file named "a.txt" inside one directory.

	> touch b.txt #It create a empty text file named "b.txt" inside one directory.

	> mkdir two   #It will make a new directory inside "one" named "two".

		> cd two  #Directory change to "two" from parent Directory

			> touch d.txt  # It will create empty text file named "d.txt" inside two.

			> mkdir three  #It will make a new directory inside "two" named "three"

			> touch e.txt  #It will create empty text file named "e.txt" inside "three".

			> mkdir four   #t will make a new directory inside "three" named "four"

				> cd four  # Directory change to "four" from parent Directory.

				> touch access.log #It will create empty log file named "access.log" inside four.

  **********************************************************************************************************

1.2) Delete all the files with the .log extension

find . -name "*.log" -delete

-->>  It will find all log files from parent diretory and all subdirectory and delete all these files.
-->>  "*.log" means whatever the name of a file but extension should be .log.


	*****************************************************************************************************


1.3) Add the following content to a.txt

> cd hello//one
> echo "Unix is a family of multitasking, multiuser computer operating systems that derive from the original AT&T Unix, development starting in the 1970s at the Bell Labs research center by Ken Thompson, Dennis Ritchie, and others.">>a.txt

--->First by "cd" we will go to desired location and by using echo command write the content in the respective file.

	******************************************************************************************************

1.4) Delete the directory named five

>cd hello
>rm -r one

--> find the desired location
--> rm: remove
--> -r : Recursively it will delete directory as well as sub directories.

	*********************************************************************************************************










